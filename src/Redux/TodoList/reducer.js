import { ADD_DATA, DELETE_DATA} from './actionTypes'
import { CLEAR_ALL } from '../Log/actionTypes'

const initialState = {
  list: [],
}

const reducer = (state = initialState, action = {}) => {
  let list = [...state.list]
  switch (action.type) {
    case ADD_DATA:
      list.splice(action.index, 0, action.text)
      return {
        ...state,
        list
      }

    case DELETE_DATA:
      list.splice(action.index, 1)
      return {
        ...state,
        list,
      }

    case CLEAR_ALL:
      return initialState

    default:
      return state
  }
}

export default reducer
