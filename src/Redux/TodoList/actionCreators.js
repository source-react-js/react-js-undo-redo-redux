import { ADD_DATA, DELETE_DATA} from './actionTypes'

export const addData = (data) => {
  return {
    type: ADD_DATA,
   ...data
  }
}

export const deleteData = (data) => {
  return {
    type: DELETE_DATA,
    ...data
  }
}