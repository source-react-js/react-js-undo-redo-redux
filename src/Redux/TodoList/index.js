import * as actionCreators from './actionCreators'
import * as actionType from './actionTypes'
import reducer from './reducer'

export const TODO_LIST = {
  actionCreators,
  actionType,
  reducer
}