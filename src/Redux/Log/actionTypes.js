export const SAVE_LOG = 'SAVE_LOG'
export const UNDO = 'UNDO'
export const REDO = 'REDO'
export const CLEAR_ALL = 'CLEAR_ALL'
