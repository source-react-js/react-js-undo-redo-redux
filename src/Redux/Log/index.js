import * as actionCreators from './actionCreators'
import * as actionType from './actionTypes'
import reducer from './reducer'

export const LOG = {
  actionCreators,
  actionType,
  reducer
}