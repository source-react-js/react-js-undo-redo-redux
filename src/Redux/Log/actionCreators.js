import { SAVE_LOG, UNDO, REDO, CLEAR_ALL } from './actionTypes'

export const saveLog = (newLog) => {
  return {
    type: SAVE_LOG,
    newLog
  }
}

export const undo = () => {
  return {
    type: UNDO
  }
}

export const redo = () => {
  return {
    type: REDO
  }
}

export const clearAll = () => {
  return {
    type: CLEAR_ALL
  }
}