import { SAVE_LOG, UNDO, REDO, CLEAR_ALL } from './actionTypes'

const initialState = {
  log: [],
  index: -1
}

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SAVE_LOG:
      let log = [...state.log, action.newLog]
      if (state.index !== state.log.length - 1) {
        log = [...state.log.slice(0, state.index + 1), action.newLog]
      }

      return {
        ...state,
        log,
        index: log.length - 1
      }

    case REDO:
      return {
        ...state,
        index: state.index + 1
      }

    case UNDO:
      return {
        ...state,
        index: state.index - 1
      }

    case CLEAR_ALL:
      return initialState

    default:
      return state
  }
}

export default reducer
