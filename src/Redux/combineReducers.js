import { combineReducers } from 'redux'
import todoListReducer  from './TodoList/reducer'
import logReducer  from './Log/reducer'
export default combineReducers({
    todoListReducer,
    logReducer,
  })
  