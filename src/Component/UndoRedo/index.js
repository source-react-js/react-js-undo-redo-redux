import React from 'react'
import './style.css'
export const UndoRedoComponent = (props) => {
  const { logReducer, clickUndo, clickRedo, clickClearAll, isDisableUndo, isDisableRedo } = props
  return (
    <div className={`content ${logReducer.log.length === 0 ? 'disable' : ''}`} >
      <div className='btn-action'>
        <div className={`undo ${isDisableUndo ? 'disable' : ''}`} onClick={clickUndo}>
          Undo
        </div>
        <div className={`redo ${isDisableRedo ? 'disable' : ''}`} onClick={clickRedo}>
          Redo
        </div>
        <div className='clear' onClick={clickClearAll}>
          Clear
        </div>
      </div>

      <div className='list-log'>
        <div className='head-log'>
          <div className='text-head' >Action</div>
          <div className='text-head' >Text</div>
        </div>
        {
          logReducer.log.map((e, i) => {
            return (
              <div className={`list ${i === logReducer.index ? 'active' : ''}`} key={`log-${i}`}>
                <div className='value'  >
                  {i + 1}{`. `}{e.redo.type}
                </div>
                <div className='value2'>
                  {e.redo.text}
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}
