import './app.css'
import { TodoList } from './Container/TodoList'
import { UndoRedo } from './Container/UndoRedo'
export const App = () => {
  return (
    <div className="app">
      <TodoList />
      <UndoRedo />
    </div>
  )
}
