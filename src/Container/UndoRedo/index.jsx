import { useDispatch, useSelector } from "react-redux"
import { LOG } from '../../Redux/index'
import { UndoRedoComponent } from '../../Component'

const { actionCreators: { undo, redo, clearAll } } = LOG

export const UndoRedo = () => {
  const logReducer = useSelector(({ logReducer }) => logReducer)

  const dispatch = useDispatch()

  const isDisableUndo = logReducer.index < 0
  const clickUndo = () => {
    if (!isDisableUndo) {
      const data = logReducer.log[logReducer.index]
      dispatch(data.undo)
      dispatch(undo())
    }
  }

  const isDisableRedo = logReducer.index === logReducer.log.length - 1
  const clickRedo = () => {
    if (!isDisableRedo) {
      const data = logReducer.log[logReducer.index + 1]
      dispatch(data.redo)
      dispatch(redo())
    }
  }

  const clickClearAll = () => {
    dispatch(clearAll())
  }
  return (
    <UndoRedoComponent
      logReducer={logReducer}
      clickUndo={clickUndo}
      clickRedo={clickRedo}
      clickClearAll={clickClearAll}
      isDisableRedo={isDisableRedo}
      isDisableUndo={isDisableUndo}
    />
  )
}