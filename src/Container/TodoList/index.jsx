import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { TODO_LIST, LOG } from '../../Redux/index'
import { TodoListComponent } from '../../Component'

const { actionCreators: { saveLog } } = LOG
const { actionCreators: { addData, deleteData } } = TODO_LIST

export const TodoList = () => {
  const { list } = useSelector(({ todoListReducer }) => todoListReducer)
  const [text, setText] = useState('')
  const dispatch = useDispatch()

  const clickAdd = () => {
    if (text) {
      const index = list.length
      setText('')
      const data = { text, index }
      const redo = addData(data)
      dispatch(redo)
      dispatch(saveLog({
        redo,
        undo: deleteData(data),
      }))
    }
  }

  const clickDelete = (index) => {
    const data = { text: list[index], index }
    const redo = deleteData(data)
    dispatch(redo)
    dispatch(saveLog({
      redo,
      undo: addData(data),
    }))
  }

  const changeText = ({ key, target }) => {
    key === "Enter" ? clickAdd() : setText(target.value)
  }

  return (
    < TodoListComponent text={text}
      list={list}
      changeText={changeText}
      clickAdd={clickAdd}
      clickDelete={clickDelete}
      disabled={list.length === 10}
    />
  )
}